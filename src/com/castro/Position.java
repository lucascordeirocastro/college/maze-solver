package com.castro;

public class Position {
    int x;
    int y;
    Position ancestor;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
        this.ancestor = null;
    }

    public Position(int x, int y, Position ancestor) {
        this.x = x;
        this.y = y;
        this.ancestor = ancestor;
    }

    // Getters
    int getX() {
        return x;
    }

    int getY() {
        return y;
    }

    Position getAncestor() {
        return ancestor;
    }
}
