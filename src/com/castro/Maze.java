package com.castro;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Maze {

    // Here are the possible directions of the hero, they translate up, right, down and left
    private static final int[][] DIRECTIONS = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

    // The declaration of the maze, then a 'visited' array to gather all the positions that were already covered,
    //and also the position of the hero and the villain that are important for future reference and validation
    private int[][] maze;
    private boolean[][] visited;
    private Position hero;
    private Position villain;

    // Constructor that calls the FileReader to populates the maze with the .txt file
    public Maze(File maze) throws FileNotFoundException {
        FileReader fileReader = new FileReader(maze);
        String fileText = fileReader.getFileText();
        initializeMaze(fileText);
    }

    // Initialization of the maze where is defined each possible property of the file that was read
    private void initializeMaze(String fileText) {

        // It's created a vector with each line of the string, the maze with height and width related to the lines vector
        //and also a vector that will be getting the positions already covered

        String[] lines = fileText.split("\n");
        maze = new int[lines.length][lines[0].length()];
        visited = new boolean[lines.length][lines[0].length()];

        // We go through each character specifying what each one is
        for (int row = 0; row < getHeight(); row++) {
            for (int col = 0; col < getWidth(); col++) {

                switch (lines[row].charAt(col)) {
                    case '#':
                        maze[row][col] = '#';
                        break;
                    case 'A':
                        maze[row][col] = 'A';
                        hero = new Position(row, col);
                        break;
                    case 'B':
                        maze[row][col] = 'B';
                        villain = new Position(row, col);
                        break;
                    case '.':
                        maze[row][col] = '.';
                        break;
                }
            }
        }
    }

    //Getters
    public int getHeight() {
        return maze.length;
    }

    public int getWidth() {
        return maze[0].length;
    }

    public Position getHeroPos() {
        return hero;
    }

    public Position getVillainPos() {
        return villain;
    }

    //Setters
    public void setVisited(int row, int col, boolean value) {
        visited[row][col] = value;
    }

    public void setMaze(int[][] maze) {
        this.maze = maze;
    }

    public void setHero(Position hero) {
        this.hero = hero;
    }

    public void setVillain(Position villain) {
        this.villain = villain;
    }

    //Is
    public boolean isVillain(int x, int y) {
        return x == villain.getX() && y == villain.getY();
    }

    public boolean isHero(int x, int y) {
        return x == hero.getX() && y == hero.getY();
    }

    public boolean isVisited(int row, int col) {
        return visited[row][col];
    }

    public boolean isWall(int row, int col) {
        return maze[row][col] == '#';
    }

    public boolean isValidLocation(int row, int col) {
        return row >= 0 && row < getHeight() && col >= 0 && col < getWidth();
    }

    public static List<Position> solve(Maze maze) {

        // To solve the maze that was received, it's created a linked list to keep the next step position, and
        //also it's needed a start position that is where the hero is
        LinkedList<Position> nextPosition = new LinkedList<>();
        Position start = maze.getHeroPos();
        nextPosition.add(start);

        // A while loop that iterates until all positions are covered or the villain is found
        while (!nextPosition.isEmpty()) {
            // We take the next position and we transform into the current to then analyze it
            Position current = nextPosition.remove();

            // if the current position is not valid or has already being visited, then nothing is done, just continue
            if (!maze.isValidLocation(current.getX(), current.getY()) || maze.isVisited(current.getX(), current.getY())) {
                continue;
            }
            // if it's a wall, then set it's position as visited and continue
            if (maze.isWall(current.getX(), current.getY())) {
                maze.setVisited(current.getX(), current.getY(), true);
                continue;
            }
            // if it's the villain, then we achieved the end of the maze, so we call a method that returns the whole path that was taken
            if (maze.isVillain(current.getX(), current.getY())) {
                return getWholePath(current);
            }
            // Add each possible direction to the nextPosition 'array' to later be analyzed
            for (int[] direction : DIRECTIONS) {
                Position position = new Position(current.getX() + direction[0], current.getY() + direction[1], current);
                nextPosition.add(position);
            }

            // Sets the current position that was analyzed as already visited
            maze.setVisited(current.getX(), current.getY(), true);
        }

        // If the whole maze was iterated but the villain wasn't found, then return an empty list
        return Collections.emptyList();
    }

    // A method to retrieve the whole path that was taken to reach the villain, by getting each position's parent
    //and add all of them into a list
    private static List<Position> getWholePath(Position current) {
        List<Position> path = new ArrayList<>();

        while (current != null) {
            path.add(current);
            current = current.getAncestor();
        }

        return path;
    }

    // A method that gets to every position of a path, and if the position is not the hero's position, then add it
    //to a pathLength integer that is counting the number of positions taken to get to the villain
    public int getPathLength(List<Position> path) {
        int pathLength = 0;

        for (Position position : path) {
            if (!isHero(position.getX(), position.getY())) {
                pathLength += 1;
            }
        }

        return pathLength;
    }
}
