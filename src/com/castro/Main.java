package com.castro;

import java.io.File;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {

        for (int x = 1; x <= 3; x++) {
            System.out.println("_________  " + x + "°  Maze _________");
            long InitialTime = System.currentTimeMillis();

            Maze maze = new Maze(new File("src/com/castro/mazes/case" + x + ".txt"));

            List<Position> path = Maze.solve(maze);

            int shortestPathLength = maze.getPathLength(path);
            System.out.println("Shortest Path = " + shortestPathLength);

            long FinalTime = System.currentTimeMillis();
            System.out.printf("%.3f ms%n", (FinalTime - InitialTime) / 1000d);

        }
    }
}
