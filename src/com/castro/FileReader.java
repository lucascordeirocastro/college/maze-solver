package com.castro;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

// File Reading Class, here the file will be read and transformed into a string
public class FileReader {
    private String fileText = "";

    public FileReader(File maze) throws FileNotFoundException {
        try (Scanner input = new Scanner(maze)) {
            while (input.hasNextLine()) {
                fileText += input.nextLine() + "\n";
            }
        }
    }

    public String getFileText() {
        return fileText;
    }

}
